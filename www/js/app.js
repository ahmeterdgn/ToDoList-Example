

Vue.component('ekle', {
  template: '\
    <li>\
      {{ baslik }} \
        <a uk-icon="icon: close" v-on:click="$emit(\'remove\')"></a>\
    </li>\
  ',
  props: ['baslik']
})

new Vue({
  el: '#app',
  data: {
    yeniyazi: '',
    todos: [],
    nextTodoId: 1,
    isloading:true
  },
  

    created(){
    setTimeout(()=>{
      this.isloading=false;

    },1000)
    
  },



  methods: {
    eklendi: function () {
      alert("eklendi");
      this.todos.push({
        id: this.nextTodoId++,
        baslik: this.yeniyazi
      })
      this.yeniyazi = ''
    }
  }
})
